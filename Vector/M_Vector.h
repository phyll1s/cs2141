#ifndef M_VECTOR
#define M_VECTOR

#include <iostream>
using namespace std;

/*
*Header file for M_Vector.cpp
*Written by: Andrew Markiewicz
*for: CS2141 Hw3
*10/30/09
*/

class M_Vector {

    public:
        M_Vector(double* orig, int s);    //Creat a vector from an array of doubles given the size
        M_Vector(int s);    //Create a vector of all zeros by only giving the size
        M_Vector();    //Create an empty vector
        M_Vector(const M_Vector & v);   //Copy constructor 
        int getSize() const;	//Accessor method for the size variables

        friend ostream& operator<< ( ostream & out, const M_Vector & v);    //The coutput operator
        double & operator[] (int x);  //Lets the double at point x be accessed or modified
        double operator[] (int x) const;  //Lets the double at point x be accessed or modified        

        const double operator* (const M_Vector & v) const;    //Multiplies 2 vectors with the dot product

        const M_Vector operator- (const M_Vector & v) const;    //Subtract one vector from another
        void operator-= (const M_Vector & v);	//Subtract one vector from another and sets the left as the answer

        const M_Vector operator+ (const M_Vector & v) const;    //Add one vector to another
        void operator+= (const M_Vector & v);	//Adds to vectors and sets the left as the sum

        const M_Vector operator/ (const double dbl) const;    //Divide vector by a double
        void operator/= (const double dbl);    //Divide vector by a double and sets left as the answer
        const M_Vector operator/ (const int I) const;    //Divide vector by a int
        void operator/= (const int I);    //Divide vector by an int and sets the left as the answer              

        const M_Vector operator* (const double dbl) const;   //Multiply vector by a double
        void operator*= (const double dbl);   //Multiply vector by a double and sets left as the answer
        const M_Vector operator* (const int I) const;   //Multiply vector by an int 
        void operator*= (const int I);   //Multiply vector by an int and sets the left as the answer

        bool operator== (const M_Vector & v) const;     //Checks to see if 2 vectors are equal
        bool operator!= (const M_Vector & v) const;     //Checks to see if 2 vectors are unequal

        const M_Vector & operator= (const M_Vector & v);	//Assignment operator overload

        ~M_Vector();	//Destructor


    private:
        int size;	//Size variable
        double* values;	//array that holds the values in the vector

};
#endif

