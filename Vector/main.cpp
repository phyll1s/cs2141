/********************************************************************
 * main.cpp
 * Version 1.1a
 * Written by Paul Bonamy
 * Last update: 14 October 2009
 * 
 * Demonstration of the proper functioning of an M_Vector class,
 * provided as part of Homeword #3 for CS2141, Fall 2009
 *
 * This program does not provide an exhaustive test of the M_Vector.
 * It is intended, rather, to provide a reasonable demonstration
 * of the class's functionality.
 *
 * While you may modify this file for the purposes of testing your
 * program, please be sure to a) submit an unmodifed copy with your
 * program and b) write your class to be independent of the details
 * of main's implementation. I should be able to use your class with
 * any version of main.cpp that respects the same class interface.
 *
 * This code should be error free. In the event that it is not,
 * please send me an email (pjbonamy@mtu.edu)
 * ******************************************************************/

#include <iostream>
#include "M_Vector.h"

using namespace std;

int main() {
    cout << "Constructor tests:" << endl;
    // creation from double array
    double a_values[] = {1,2,3,4,5,6};
    M_Vector a(a_values, 6);
    cout << "M_Vector a should have (1, 2, 3, 4, 5, 6). It actually has: " << a << endl;
    
    // zero fill
    M_Vector b(6);
    cout << "M_Vector b should have (0, 0, 0, 0, 0, 0). It actually has: " << b << endl;
    
    // empty
    M_Vector c;
    cout << "M_Vector c should have (). It actually has: " << c << endl;
    
    // copy
    M_Vector d(b);
    cout << "M_Vector d should have (0, 0, 0, 0, 0, 0). It actually has: " << d << endl;
    
    // asignment test
    cout << endl << "Assignment test:" << endl;
    M_Vector k;
    k = a;
    cout << "M_Vector k should have (1, 2, 3, 4, 5, 6). It actually has: " << k << endl;
    
    // [] test
    cout << endl << "Operator [] test:" << endl;
    for (int i = 0; i < a.getSize(); i++) {
        b[b.getSize() - i - 1] = a[i];
    }
    cout << "b should now have (6, 5, 4, 3, 2, 1). It actually has: " << b << endl;
    cout << "d should now have (0, 0, 0, 0, 0, 0). It actually has: " << d << endl;
    
    cout << endl << "M_Vector-M_Vector mathematics tests:" << endl;
    // dot product
    double e = a * b;
    cout << "e should have value 56. It actually has: " << e << endl;
    
    // addition
    M_Vector f = a + b;
    cout << "f should now have (7, 7, 7, 7, 7, 7). It actually has: " << f << endl;
    f += a;
    cout << "f should now have (8, 9, 10, 11, 12, 13). It actually has: " << f << endl;
    M_Vector g = a + c;
    cout << "g should have value (). It actually has: " << g << endl;
    g += a;
    cout << "g should have value (). It actually has: " << g << endl;
    
    // subtraction
    M_Vector h = a - b;
    cout << "h should now have (-5, -3, -1, 1, 3, 5). It actually has: " << h << endl;
    h -= a;
    cout << "h should now have (-6, -5, -4, -3, -2, -1). It actually has: " << h << endl;
    M_Vector i = a - c;
    cout << "i should have value (). It actually has: " << i << endl;
    i += a;
    cout << "i should have value (). It actually has: " << i << endl;
    
    cout << endl << "M_Vector-Scalar mathematics tests:" << endl;
    // multiplication
    a = a * 2;
    cout << "a should now have (2, 4, 6, 8, 10, 12). It actually has: "  << a << endl;
    a *= 2;
    cout << "a should now have (4, 8, 12, 16, 20, 24). It actually has: "  << a << endl;
    
    // division
    a = a / 4;
    cout << "a should now have (1, 2, 3, 4, 5, 6). It actually has: "  << a << endl;
    a /= 0.5;
    cout << "a should now have (2, 4, 6, 8, 10, 12). It actually has: "  << a << endl;
    cout << "k should now have (1, 2, 3, 4, 5, 6). It actually has: "  << k << endl;
    
    cout << endl << "Equality tests:" << endl;
    M_Vector j(6);
    cout << "a == b should be 0. It's actually: " << (a == b) << endl;
    cout << "a == g should be 0. It's actually: " << (a == g) << endl;
    cout << "d == j should be 1. It's actually: " << (d == j) << endl;
    cout << "g == i should be 1. It's actually: " << (g == i) << endl;
    cout << "a != b should be 1. It's actually: " << (a != b) << endl;
    cout << "a != g should be 1. It's actually: " << (a != g) << endl;
    cout << "d != j should be 0. It's actually: " << (d != j) << endl;
    cout << "g != i should be 0. It's actually: " << (g != i) << endl;
    
    return 0;
}