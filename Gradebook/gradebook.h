		#ifndef GRADEBOOK_H_
		#define GRADEBOOK_H_
		#include <vector>
		#include <string>
		
		class Gradebook {
			
			public:
			
				Gradebook();	//Default constructor
				
				void mainMenu();	//Main menu of the app
				void openMenu();	//Open menu of the app
				void createMenu();	//Create menu of the app
				void editMenu();	//Edit menu of the app
				
				void addStudent (string last, string first);	//Adds a new student to the gradebook
				void addAssignment (int max);	//Adds a new assignment to the gradebook
				void gradeAssignment();	//Changes grade for 1 assignment to all students
				void singleGradeChange();	//Changes grade on an assignment for 1 student
				void gradeReport();	//Displays a grade report for the whole class
				void singleGradeReport();	//Displays a grade report for a single student
				
				void writeFiles();	//Writes a series of files that store all info
				void readFiles();	//Reads a series of files that store all info
				
				void setAssignment (int assignment, int max);	//Sets the max grade on an assignment
				
				~Gradebook(){this->empty();} //Destructor
			
			
			private:
			
				class Student{			
								
					public:
						Student();
						Student(string last, string first, int numAssig);
						string getFirstName(){return firstName;}
						string getLastName(){return lastName;}
						void setAssignment (int assignment, int grade);	//Sets a students grade on an assignment
						int getGrade(int assignmentNumber);	//Gets a grade for the given assignment
							
					private:
						int assignmentGrades[15];
						string firstName;
						string lastName;
						int total;
				};
				
				std::vector<Student> studentVector;
				int assignments[15];
				int numAssignments;
				int totalPoints;
				void empty();			
		};
		
		#endif /*GRADEBOOK_H_*/
