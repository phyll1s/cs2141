	#include <iostream>
	#include <cstdlib>
	#include <iomanip>
	#include <string>
	using std::string;
	#include "gradebook.h"
	#include <fstream> 
	using namespace std;
	
	/*
	*Gradebook Class
	* Written by: Andrew Markiewicz
	* for: CS2141 Term Project
	* Last Edited: 12/11/09
	*/
	
	/*
	 * Constructor
	 */
	Gradebook::Gradebook(){
		numAssignments=0;
		totalPoints=0;
		for(int i=0; i<15; i++){
			assignments[i]=NULL;
		}
	}
	
	/*
	 *Main menu portion of the gradebook 
	 */
	void Gradebook::mainMenu(){
		int mainMenuInt;
		system("clear");
		cout<<"Gradebook App V. 1"<<endl; 
		cout<<"--------------------"<<endl;
		cout<<"Main Menu"<<endl;
		cout<<"--------------------"<<endl;
		cout<<"1.Open a gradebook"<<endl;
		cout<<"2.Create a gradebook"<<endl;
		cout<<"3.Exit"<<endl;
		cout<<"--------------------"<<endl;
		cout<<"Enter your choice: "<<endl;
		
		cin>>mainMenuInt;
		
		if(mainMenuInt==1){
			openMenu();
			
		}
		
		if(mainMenuInt==2){
			createMenu();
		}
	
		if(mainMenuInt==3){
			system("clear");
			//Delete all data
			return;
		}
			
	}
	
	void Gradebook::openMenu(){
		writeFiles();
		mainMenu();
		/*
		int openMenuInt;void openMenu();
		system("clear");
		cout<<"Open Menu"<<endl;
		cout<<"-----------------"<<endl;
		cout<<"1.Open the gradebook"<<endl;
		cout<<"2.Main Menu"<<endl;
		cout<<"Enter your choice: "<<endl;
		cin>>openMenuInt;
		
		if(openMenuInt==1){
			//readFiles();
			editMenu();
			
		}
		
		if(openMenuInt==2){
			mainMenu();
		}
		*/
	}
	
	void Gradebook::createMenu(){
		int createMenuInt;
		system("clear");
		cout<<"Create Menu"<<endl;
		cout<<"-----------------"<<endl;
		cout<<"1.Create a new gradebook"<<endl;
		cout<<"2.Main Menu"<<endl<<endl;
		cout<<"Enter your choice: ";
		cin>>createMenuInt;
		
		if(createMenuInt==1){
			editMenu();
			
		}
		
		if(createMenuInt==2){
			mainMenu();	
		}
	}
	
	void Gradebook::editMenu(){
		int editMenuInt;
		
		//Check if the file was opened properly
		system("clear");
		cout<<"Edit Menu"<<endl;
		cout<<"----------------------------------"<<endl;	
		cout<<"1.Add student to the class"<<endl;
		cout<<"2.Add assignment to the class"<<endl;
		cout<<"3.Assign grade to all students for an assignment"<<endl;
		cout<<"4.Change a student's grade on an assignment"<<endl;
		cout<<"5.Display grade report for entire class"<<endl;
		cout<<"6.Display grade report for a single student"<<endl;
		cout<<"7.Save gradebook"<<endl;
		cout<<"8.Main Menu"<<endl<<endl;
		cout<<"Enter your choice: ";
		cin>>editMenuInt;
		
		//Adding a student
		if(editMenuInt==1){
						
			string first;
			string last;
			
			cout<<"Last Name: ";
			cin>>last;
			
			cout<<"First Name: ";
			cin>>first;
	
			addStudent(last,first);
			
			editMenu();
			
			
		}
		
		//Add an assignment
		if(editMenuInt==2){
			int newMax;
			cout<<"Assignment "<<numAssignments<<" will be added"<<endl;
			cout<<"What is the max grade possible? ";
			cin>>newMax;
			addAssignment(newMax);
			editMenu();
			
				
		}
		
		//Assign grade to all students
		if(editMenuInt==3){
			gradeAssignment();
			editMenu();
		}
			
		//Change a students grade
		if(editMenuInt==4){
			singleGradeChange();
			editMenu();
			
		}
			
		//Display grade report
		if(editMenuInt==5){
			gradeReport();
			editMenu();
			
		}
		
		//Display grade report for single student
		if(editMenuInt==6){
			singleGradeReport();
			editMenu();
			
		}
			
		//Saves gradebook
		if(editMenuInt==7){
			writeFiles();
			editMenu();
		}
			
		//Main menu
		if(editMenuInt==8){
			mainMenu();
		}
			
	}
	
	//Adds a new student to the list
	void Gradebook::addStudent (string last, string first){
		Student * toAdd = new Student(last, first, numAssignments);
		
		//If the inserted student if the first student
		if(studentVector.size() == 0){
			studentVector.push_back(*toAdd);
			return;
		}
		
		//If new student belongs in front or middle
		vector<Student>::iterator itr = studentVector.begin();
		for(int i=0; i<studentVector.size(); i++){
			if(toAdd->getLastName().compare(studentVector[i].getLastName()) == -1 || ( toAdd->getLastName().compare(studentVector[i].getLastName()) == 0  && toAdd->getFirstName().compare(studentVector[i].getFirstName()) == -1 )){
				studentVector.insert(itr, *toAdd);
				return;
			}
			itr++;
		}
		
		//If not inserted yet, insert at end
		studentVector.push_back(*toAdd);
		return;		
		
	}
	
	
	void Gradebook::addAssignment (int max){
		assignments[numAssignments] = max;
		for(int i=0; i<studentVector.size(); i++){
			studentVector[i].setAssignment(numAssignments, 0);	
		}
		totalPoints+=max;
		numAssignments++;
		
	}
	
	//Changes the grade on all students for 1 assignment
	void Gradebook::gradeAssignment(){
		int tempGrade;
			int assignmentNumber;
			char oops;
			if(studentVector.size() == 0){
				cout<<"No students yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			if(numAssignments == 0){
				cout<<"No assignments yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			
			cout<<"Changing grade for each student, which assignment? ";
			cin>>assignmentNumber;
			if(assignmentNumber > numAssignments-1){
				cout<<"That assignment doesn't exist yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			
			cout<<"Alright, now enter a grade for each student - press enter after each grade"<<endl;
			cout<<"Max points: "<<assignments[assignmentNumber]<<endl;
			for(int i=0; i<studentVector.size(); i++){
					cout<<studentVector[i].getLastName()<<", "<<studentVector[i].getFirstName()<<": ";
					cin>>tempGrade;
					studentVector[i].setAssignment(assignmentNumber, tempGrade);
			}
			return;
			
		
	}
	
	//Changes an assignment grade for a single student
	void Gradebook::singleGradeChange(){
		char oops;
			if(studentVector.size() == 0){
				cout<<"No students yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			if(numAssignments == 0){
				cout<<"No assignments yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			
			string sFirstName;
			string sLastName;
			cout<<"Changing a students grade, what student? "<<endl;
			cout<<"Last Name: ";
			cin>>sLastName;
			cout<<"First Name: ";
			cin>>sFirstName;
			cout<<endl;
			
			int assignmentNumber;
			int newGrade;
			for(int i=0; i<studentVector.size(); i++){
				if(studentVector[i].getLastName()==sLastName && studentVector[i].getFirstName()==sFirstName){
					cout<<"Student found!"<<endl;
					cout<<"What assignment? ";
					cin>>assignmentNumber;
					
					if(assignmentNumber > numAssignments-1){
						cout<<"That assignment doesn't exist yet! Press y and enter to continue"<<endl;
						cin>>oops;
						return;
					}
					cout<<"Max points: "<<assignments[assignmentNumber];
					cout<<endl;
					cout<<"New Grade: ";
					cin>>newGrade;
					studentVector[i].setAssignment(assignmentNumber, newGrade);
					return;
				}
				cout<<"Student not found! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			
			}

			
		return;
		
	}
	
	void Gradebook::gradeReport(){
		system("clear");
				
		//Centers 'student name'
   		for(int i=0;i<10;i++)
    		cout<<" ";

		//Draws Collumn Header
		cout<<"Student Name";
		for(int i=0;i<11;i++)
    		cout<<" ";
    		
		cout<<"|";
		for(int i=0; i<numAssignments; i++){
			cout<<" Assign. "<<i<<"(/"<<assignments[i]<<")" <<" |";
		}
		cout<<" Total Grade";
		cout<<endl;
		
		//Draws Line
		for(int j=0; j<(55+20*numAssignments-1); j++){
			cout<<"-";
		}
		cout<<endl;
		
		//Outputs Students info
		for(int k=0; k<studentVector.size(); k++){
			//Outputs student name
			string temp = studentVector[k].getLastName() + ", " + studentVector[k].getFirstName();
			cout.width(33);
			cout.setf( ios::left );
			cout<<temp;
			
			//Outputs Grade
			for(int u=0; u<numAssignments; u++){
				//Centers grade 
				for(int t=0; t<8; t++){
					cout<<" ";
				}			
				cout<<studentVector[k].getGrade(u);
				for(int t=0; t<7; t++){
					cout<<" ";
				}
				
			}
			double temp1=0;
			double temp2=0;
			for(int i=0; i<numAssignments; i++){
				temp1+=studentVector[k].getGrade(i);
				temp2+=assignments[i];
			}
			cout<<"      "<<temp1/temp2;
			cout<<endl;	
		}
		//Draws bottom line
		for(int j=0; j<(55+20*numAssignments-1); j++){
			cout<<"-";
		}
		cout<<endl;
		
		//Average Row
		cout<<"Average:";
		for(int v=0; v<25; v++){
			cout<<" ";
		}
		
		
		//Computes Average Assignment Grade
		for(int b=0; b<numAssignments; b++){
			for(int q=0; q<7; q++){
				cout<<" ";
			}
			double tempSum;
			
			for(int c=0; c<studentVector.size(); c++){
				tempSum+=studentVector[c].getGrade(b);
			}
			double tempAvg=tempSum/(studentVector.size()*assignments[b]);
			cout<<tempAvg;
			
			tempSum=0;
			
			for(int q=0; q<8; q++){
				cout<<" ";
			}
			
		}
		cout<<endl;
		char oops;
		cout<<"Press y and hit enter when done ";
		cin>>oops;
		return;	
	}
	
	void Gradebook::singleGradeReport(){
		char oops;
			if(studentVector.size() == 0){
				cout<<"No students yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			if(numAssignments == 0){
				cout<<"No assignments yet! Press y and enter to continue"<<endl;
				cin>>oops;
				return;
			}
			
			string sFirstName;
			string sLastName;
			cout<<"View a Students grade report, what student? "<<endl;
			cout<<"Last Name: ";
			cin>>sLastName;
			cout<<"First Name: ";
			cin>>sFirstName;
			cout<<endl;
			
			for(int p=0; p<studentVector.size(); p++){
				if(studentVector[p].getLastName()==sLastName && studentVector[p].getFirstName()==sFirstName){
					
							
				//Centers 'student name'
   				for(int i=0;i<10;i++)
    				cout<<" ";

				//Draws Collumn Header
				cout<<"Student Name";
				for(int i=0;i<11;i++)
    				cout<<" ";
    		
				cout<<"|";
				
				for(int i=0; i<numAssignments; i++){
					cout<<" Assign. "<<i<<"(/"<<assignments[i]<<")" <<" |";
				}
		
				cout<<" Total Grade";
				cout<<endl;
		
				//Draws Line
				for(int j=0; j<(55+20*numAssignments-1); j++){
					cout<<"-";
				}
				
				cout<<endl;
		
				//Outputs Students info
				
				//Outputs student name
				string temp = studentVector[p].getLastName() + ", " + studentVector[p].getFirstName();
				cout.width(33);
				cout.setf( ios::left );
				cout<<temp;
			
				//Outputs Grade
				for(int u=0; u<numAssignments; u++){
					//Centers grade 
					for(int t=0; t<8; t++){
						cout<<" ";
					}			
					cout<<studentVector[p].getGrade(u);
					for(int t=0; t<7; t++){
						cout<<" ";
					}
					
				}
				
				//Outputs overal grade
				double temp1=0;
				double temp2=0;
				for(int i=0; i<numAssignments; i++){
					temp1+=studentVector[p].getGrade(i);
					temp2+=assignments[i];
				}
				cout<<"      "<<temp1/temp2;
				cout<<endl;	
				
				//Draws bottom line
				for(int j=0; j<(55+20*numAssignments-1); j++){
					cout<<"-";
				}
				cout<<endl;
				cout<<"Press y and hit enter when done ";
				cin>>oops;
				return;
			}		
		}
		cout<<"Student not found! Press y and enter to continue"<<endl;
		cin>>oops;
		return;
			
			}
		
	void Gradebook::writeFiles(){
		system("clear");
		cout<<"Lawl sorry, feature not implemented yet. Press y and hit enter to go back"<<endl;
		char oops;
		cin>>oops;
		return;		
	}
	
	//Frees memory
	void Gradebook::empty(){

	}
	
	
	Gradebook::Student::Student(string last, string first, int numAssig){
		//When student added, starts w/ 0's on existing assignments
		for(int i=0; i<numAssig; i++){
			assignmentGrades[i]=0;
		}
		
		lastName = last;
		firstName = first;
	}
	
	//Sets a students grade on an assignment
	void Gradebook::Student::setAssignment (int assignment, int grade){
		assignmentGrades[assignment] = grade;
		total+=grade;
	}
	
	//Returns students grade for a given assignment
	int Gradebook::Student::getGrade(int assignmentNumber){		
		return assignmentGrades[assignmentNumber];
	}