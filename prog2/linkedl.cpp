#include <iostream>
#include "linkedl.h"
using namespace std;

/*
*Linked List class
*Written by: Andrew Markiewicz
*for: CS2141
*/


//Constructor
LinkedL::LinkedL(){
    size=0;
}

/*
*Method that prints out all the elements in the list
*Returns nothing, accepts nothing
*/
void LinkedL::printAll(){
    //Prints out empty if the list is empty
    if(size==0){
        cout<<"Empty"<<endl;
        return;
    }
    
    //Case for only one element in the list
    if(size==1){
        cout<<*first->getElement()<<endl;
        return;
    }
    
    //Case for multiple elements in the list
    Node * current = first;
    while(size>1){
        cout<<*current->getElement()<<" ";
        if(current->getNext()==NULL){
            cout<<endl;
            return;
        }
        else
            current = current->getNext();
    }
}


/*
*Method to empty the linked list
*Returns nothing, accepts nohing
*/
void LinkedL::empty(){
   //If the list is already empty
    if(size==0)
       return;
    //If the list only has one item
    if(size==1){
        delete first;
        first = NULL;
        last = NULL;
        size--;
        return;
    }
    //If the list has >1 item
    Node * current = first;
    Node * next;
    while(current->getNext()!=last){
        next = current->getNext();
        delete current;
        size--;
        current = next;
    }
    delete current;
    delete last;
    size=size-2;
    first = NULL;
    last = NULL;
}

/*
*Method to add an integer to the list
*returns Nothing
*Accepts a pointer to the new int to enter
*/
void LinkedL::add(int *toAdd){
    //Case if the list was previously empty
    Node *newNode = new Node(toAdd);
    if(size==0){
        newNode->setPrev(NULL);
        newNode->setNext(NULL);
        size++;
        first = newNode;
        last = newNode;
        return;
    }
    
    //Case if the new entry belongs at the begining of the list
    if(*toAdd<=*first->getElement()){
        Node * currentFirst = first;
        first = newNode;
        newNode->setNext(currentFirst);
        newNode->setPrev(NULL);
        currentFirst->setPrev(newNode);
        size++;
        return;
    }
    
    //Case if the entry belongs anywhere else
    else{
        Node * current = first;
        while(*toAdd>=*current->getElement()){
            //If it belongs at thend
            if(current->getNext()==NULL){
                current->setNext(newNode);
                newNode->setPrev(current);
                newNode->setNext(NULL);
                size++;
                last = newNode;
                return;
            }
            current = current->getNext();
        }
        //Adds in the "middle" of the list
        current->getPrev()->setNext(newNode);
        newNode->setPrev(current->getPrev());
        current->setPrev(newNode);
        newNode->setNext(current);
        size++;
        return;
    }
}

/*
*Method to remove all instances of an integer from the list
*Returns nothing
*Accepts a pointer to an int to remove
*/
void LinkedL::remove(int *toRemove){
    //If list is empty
    if(size==0)
        return;
    //If there is only one element in the list
    if(size==1 && *first->getElement()==*toRemove){
        delete first;
        size--;
        first=NULL;
        last=NULL;
        return;
    }
    //For lists with size>1
    Node * current = first;
    while(current!=NULL){
        if(*current->getElement()==*toRemove){
            //If it's the first that's being removed
            if(current==first){
                Node * temp = current->getNext();
                //When deleting the last element
                if(temp!=NULL)
                    temp->setPrev(NULL);
                first = temp;
                delete current;
                size--;
            }
            //If it's the last that's being removed
            else if(current==last){
                Node * temp = current->getPrev();
                temp->setNext(NULL);
                last=temp;
                delete current;
                size--;
            }
            else{
                //deleting anywhere in the middle of the list
                Node * tempPrev = current->getPrev();
                Node * tempNext = current->getNext();
                tempPrev->setNext(tempNext);
                tempNext->setPrev(tempPrev);
                delete current;
                size--;
            }
        }
    current=current->getNext();     
    }
    delete toRemove;
    return;
}
