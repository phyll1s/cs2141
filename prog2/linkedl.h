#ifndef LINKEDL_H
#define LINKEDL_H
/*
*Header file for linkedl.cpp
*Written by: Andrew Markiewicz
*for: CS2141 Hw2
*9/29/09
*/

class LinkedL{
    public:
        LinkedL(); //Default constructor
        void empty(); //Empties the linked list
        void printAll();   //Prints out the entire linked list
        void add(int *toAdd);   //Inputs a new int into the linked list
        void remove(int *toRemove); //Removes an int from the linked list
        ~LinkedL(){this->empty();}; //Destructor
    private:
        int size; //# of elements in the linked list

        class Node{
            public:
                void setPrev(Node * p){prev = p;} //Sets the previous node
                void setNext(Node * n){next = n;} //Sets the next node
                Node* getPrev(){return prev;} //Returns the previous node
                Node* getNext(){return next;} //Returns the next node
                int* getElement(){return element;} //Returns the element
                Node(int *e){element = e;} //Constructor that initializes the node w/ an int
                ~Node(){delete element;} //Destructor
            private:
                Node * prev; //Previous node in the list
                Node * next; //Next node in the list
                int * element; //The int this node holds
        };
        Node * first; //The first node
        Node * last; //The last node

};


#endif
