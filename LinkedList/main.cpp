#include <iostream>
#include "linkedl.h"

using namespace std;

int main(){
    
    char input;
    int stop = -1;
    
    LinkedL * linkedList = new LinkedL();
    
    //Processes user input
    while(stop == -1){
        cin>>input;

        if(input == 'e'){
            linkedList->empty();
        }

        if(input == 'p'){
            linkedList->printAll();
        }

        if(input == 'i'){
            int * toAdd = new int;
            cin >> *toAdd;
            linkedList->add(toAdd);
        }

        if(input == 'd'){
            int * toRemove = new int;
            cin >> * toRemove;
            linkedList->remove(toRemove);
        }
   
        if(input == 'q'){
            delete linkedList;
            break;
         }
    }

    return 0;
}
