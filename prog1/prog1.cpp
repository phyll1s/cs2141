/*
*prog.cpp
*Andrew Markiewicz
*Program 1
*
*This file's purpose is to recieve and integer input, and output ascii art scaled by that input.
*/

#include <iostream>
using namespace std;

#include "funcs.h"

int main() {
	
    //Creates new array
    char **asciiArt = new char*[5];
	
    //Initializes the array's second dimension		
    for(int h=0; h<5; h++){
        asciiArt[h] = new char[5];
    }
	
    //Populates the array with the ascii art

        asciiArt[0][0] = '_';
        asciiArt[0][1] = '_';
        asciiArt[0][2] = '_';
        asciiArt[0][3] = '_';
        asciiArt[0][4] = '_';

        asciiArt[1][0] = '{';
        asciiArt[1][1] = 'o';
        asciiArt[1][2] = ',';
        asciiArt[1][3] = 'o';
        asciiArt[1][4] = '}';

        asciiArt[2][0] = '(';
        asciiArt[2][1] = '_';
        asciiArt[2][2] = '_';
        asciiArt[2][3] = '(';
        asciiArt[2][4] = '\\';

        asciiArt[3][0] = '-';
        asciiArt[3][1] = '"';
        asciiArt[3][2] = '-';
        asciiArt[3][3] = '"';
        asciiArt[3][4] = '-';

        asciiArt[4][0] = 'O';
        asciiArt[4][1] = 'R';
        asciiArt[4][2] = 'L';
        asciiArt[4][3] = 'Y';
        asciiArt[4][4] = '?';

    //Prints out the original picture
    for(int i = 0; i<5 ; i++){
        for(int j=0; j<5; j++){
            cout << asciiArt[i][j];
        }
    cout << endl;
    }	


    //creates the size variable, and asks for an input for the size of the ascii art
    int size;
    cout << "Please input an integer:";
    cin >> size;
    cout << endl;

    //Calls the multiply function which takes art and enlarges it by a scale of 'size' unless size is 0, then it exists
    while(size>0){
        multiply(asciiArt, size);
        cout << "Please input an integer:";
        cin >> size;	
    }

    return 0;


}
