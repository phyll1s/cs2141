/*
*funcs.cpp
*Andrew Markiewicz
*Program 1
*
*This file's purpose is that of a function that recives a 2D array and an integer and outputs the array scaled by the int 
*/

#include <iostream>
using namespace std;

#include "funcs.h"
/*
*This function recives a 2D array and an integer and outputs the array scaled by the int 
*Params: array - The 2D array to enlarge. multiplier - The integer to enlarge by
*Returns void.
*/
void multiply(char** array, int multiplier){

    //Goes through every row in the array
    for(int i=0; i<5; i++){
		
        //Runs through the current row as many times as the multiplier
        for(int p=0; p<multiplier; p++){
			
            //Goes through every column
            for(int j=0; j<5; j++){
				
                //Goes through every column as many times as the multiplier
                for(int k=0; k<multiplier; k++){
					
                    //Outputs each character
                    cout << array[i][j];
                }
            }
            //Gives a line break where needed.
            cout << endl;
        }
    }

}



