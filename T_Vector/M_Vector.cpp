#include <iostream>
#include "M_Vector.h"
/*Math based vector created that holds doubles
 * Created by Andrew Markiewicz
 * For: Prog3 of CS2141
 * 10/30/09
 */

using namespace std;

    //Creat a vector from an array of doubles given the size
    M_Vector::M_Vector(double* orig, int s){
        values = new double[s];
        for(int i=0; i<s; i++){
            values[i] = orig[i];
        }
        size = s;
    }

    //Create a vector of all zeros by only giving the size
    M_Vector::M_Vector(int s){
        values = new double[s];
        for(int i=0; i<s; i++){
            values[i]=0;
        }
        size=s;
    }
    
    //Create an empty vector
    M_Vector::M_Vector(){
        size=0;
        values = 0;
    }

    //Copy constructor
    M_Vector::M_Vector(const M_Vector & v){
        values = new double[v.getSize()];
        for(int i=0; i<v.getSize(); i++){
            values[i] = v[i];
        }
        size=v.getSize();
    }


    //Method to get the size
    int M_Vector::getSize() const{
        return size;
    }    

    //The coutput operator
    ostream& operator<< ( ostream & out, const M_Vector & v){
        out << "(";
        for(int i=0; i<v.getSize(); i++){
            if(i==v.getSize()-1){
               out << v[i];
               break;
            }           
             out << v[i] << ", ";
        }
        out << ")" << endl;
        return out;
    }    

    //Lets the double at point x be accessed or modified
    double & M_Vector::operator[] (int x){
        return values[x];
    }  

    //Lets the double at point x be accessed or modified
    double M_Vector::operator[] (int x) const{
        return values[x];
    }  



    //Multiplies 2 vectors with the dot product
    const double M_Vector::operator* (const M_Vector & v) const{
        if(size!=v.getSize())
            return 0;        
        double productArray[size];
        for(int i=0; i<size; i++){
            productArray[i] = values[i]*v[i];
        }
        int sum =0;
        for(int i=0; i<size; i++){
            sum = sum + productArray[i];
        }
        return sum;
    }            
    




    //Subtract one vector from another
    const M_Vector M_Vector::operator- (const M_Vector & v) const{
        if(size!=v.getSize())   
            return M_Vector();
        double returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]-v[i];
        }
        return M_Vector(returnArray, size);
    }   
    

    //Subtrats the left and right vectors and sets the left vector to the difference
    void M_Vector::operator-= (const M_Vector & v){
        if(size!=v.getSize())
            return;
        for(int i=0; i<size; i++){
            values[i] = values[i]-v[i];
        }
}	 






    //Add one vector to another
    const M_Vector M_Vector::operator+ (const M_Vector & v) const{
        if(size!=v.getSize())
            return M_Vector();
        double returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]+v[i];
        }
        return M_Vector(returnArray, size);
    }   
                        

    //Adds the left and right vectors together and sets the left vector to the sum
     void M_Vector::operator+= (const M_Vector & v){
        if(size!=v.getSize())
            return;
        for(int i=0; i<size; i++){
           values[i] = values[i]+v[i];
        }
    } 






    //Divide vector by a double
    const M_Vector M_Vector::operator/ (const double dbl) const{
        if(dbl==0 || dbl==-0)
            return M_Vector();        
        double returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]/dbl;
        }
        return M_Vector(returnArray, size);     
    }
    
    //Divide vector by a double and sets the left vector as the answer
    void M_Vector::operator/= (const double dbl){ 
        if(dbl==0 || dbl==-0)
            return;        
        for(int i=0; i<size; i++){
            values[i] = values[i]/dbl;
        }
    }

    //Divide vector by an int
    const M_Vector M_Vector::operator/ (const int I) const{
        if(I==0)
            return M_Vector();
        double returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]/I;
        }
        return M_Vector(returnArray, size);
        
    }    
    
    //Divide vector by an int and sets the left vector as the answer
    void M_Vector::operator/= (const int I){
        if(I==0)
            return;
        for(int i=0; i<size; i++){
            values[i] = values[i]/I;
        }
    }                  






    //Multiply vector by a double
    const M_Vector M_Vector::operator* (const double dbl) const{
        double returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]*dbl;
        }
        return M_Vector(returnArray, size);
    }

    //Multiply vector by a double and sets the left vector as the product
    void M_Vector::operator*= (const double dbl){
        for(int i=0; i<size; i++){
            values[i] = values[i]*dbl;
        }
    }

    //Multiply vector by an int    
    const M_Vector M_Vector::operator* (const int I) const{
        double returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]*I;
        }
        return M_Vector(returnArray, size);
    }   

    //Multiply vector by an int and sets the letf vector as the product
    void M_Vector::operator*= (const int I){
        for(int i=0; i<size; i++){
            values[i] = values[i]*I;
        }
    }




    //Checks to see if 2 vectors are equal
    bool M_Vector::operator== (const M_Vector & v) const{
        if(size!=v.getSize())  
            return false;        
        bool equal = true;
        for(int i=0; i<size; i++){
            if(values[i]!=v[i]){
                equal = false;
                break;
            }
        }
        return equal;
    }     

    //Checks to see if 2 vectors are unequal
    bool M_Vector::operator!= (const M_Vector & v) const{

        return !(*this==v);
    }     





    //Assigns one vector to another
    const M_Vector&  M_Vector::operator= (const M_Vector & v){
        delete[] values;        
        values = new double[v.getSize()];
        for(int i=0; i<v.getSize(); i++){
            values[i] = v[i];
        }
        size=v.getSize();
        
        return *this;   
    }

    //Destructor
    M_Vector::~M_Vector(){
        delete[] values;
    }

