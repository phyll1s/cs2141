#ifndef M_VECTOR
#define M_VECTOR

#include <iostream>
using namespace std;

/*
*M_Vector that takes advantage of templates
*Written by: Andrew Markiewicz
*for: CS2141 Hw4
*11/20/09
*/

template <typename T> class M_Vector {

    public:
        M_Vector(T* orig, int s);    //Creat a vector from an array of doubles given the size
        M_Vector(int s);    //Create a vector of all zeros by only giving the size
        M_Vector();    //Create an empty vector
        M_Vector(const M_Vector<T> & v);   //Copy constructor 
        int getSize() const;	//Accessor method for the size variables

        template<typename U> friend ostream& operator<< ( ostream & out, const M_Vector<U> & v);    //The coutput operator
        T & operator[] (int x);  //Lets the double at point x be accessed or modified
        T operator[] (int x) const;  //Lets the double at point x be accessed or modified        

        const T operator* (const M_Vector<T> & v) const;    //Multiplies 2 vectors with the dot product

        const M_Vector<T> operator- (const M_Vector<T> & v) const;    //Subtract one vector from another
        void operator-= (const M_Vector<T> & v);	//Subtract one vector from another and sets the left as the answer

        const M_Vector<T> operator+ (const M_Vector<T> & v) const;    //Add one vector to another
        void operator+= (const M_Vector<T> & v);	//Adds to vectors and sets the left as the sum

        const M_Vector<T> operator/ (const T dbl) const;    //Divide vector by a double
        void operator/= (const T dbl);    //Divide vector by a double and sets left as the answer
        const M_Vector<T> operator/ (const int I) const;    //Divide vector by a int
        void operator/= (const int I);    //Divide vector by an int and sets the left as the answer              

        const M_Vector<T> operator* (const T dbl) const;   //Multiply vector by a double
        void operator*= (const T dbl);   //Multiply vector by a double and sets left as the answer
        const M_Vector<T> operator* (const int I) const;   //Multiply vector by an int 
        void operator*= (const int I);   //Multiply vector by an int and sets the left as the answer

        bool operator== (const M_Vector<T> & v) const;     //Checks to see if 2 vectors are equal
        bool operator!= (const M_Vector<T> & v) const;     //Checks to see if 2 vectors are unequal

        const M_Vector<T> & operator= (const M_Vector<T> & v);	//Assignment operator overload
        
        const void changeVals(T* newArray){values=newArray;};
        const void changeSize(int newSize){size=newSize;};

        ~M_Vector();	//Destructor


    private:
        int size;	//Size variable
        T* values;	//array that holds the values in the vector

};

using namespace std;

    //Creat a vector from an array of doubles given the size
    template <typename T> M_Vector<T>::M_Vector(T* orig, int s){
        values = new T[s];
        for(int i=0; i<s; i++){
            values[i] = orig[i];
        }
        size = s;
    }

    //Create a vector of all zeros by only giving the size
    template <typename T> M_Vector<T>::M_Vector(int s){
        values = new T[s];
        for(int i=0; i<s; i++){
            values[i]=0;
        }
        size=s;
    }
    
    //Create an empty vector
    template <typename T> M_Vector<T>::M_Vector(){
        size=0;
        values = 0;
    }

    //Copy constructor
    template <typename T> M_Vector<T>::M_Vector(const M_Vector<T> & v){
        values = new T[v.getSize()];
        for(int i=0; i<v.getSize(); i++){
            values[i] = v[i];
        }
        size=v.getSize();
    }


    //Method to get the size
    template <typename T> int M_Vector<T>::getSize() const{
        return size;
    }    

    //The coutput operator
    template <typename U> ostream& operator<< ( ostream & out, const M_Vector<U> & v){
        out << "(";
        for(int i=0; i<v.getSize(); i++){
            if(i==v.getSize()-1){
               out << v[i];
               break;
            }           
             out << v[i] << ", ";
        }
        out << ")" << endl;
        return out;
    }    

    //Lets the double at point x be accessed or modified
    template <typename T> T & M_Vector<T>::operator[] (int x){
        return values[x];
    }  

    //Lets the double at point x be accessed or modified
    template <typename T> T M_Vector<T>::operator[] (int x) const{
        return values[x];
    }  



    //Multiplies 2 vectors with the dot product
    template <typename T> const T M_Vector<T>::operator* (const M_Vector<T> & v) const{
        if(size!=v.getSize()){
	        if(size<v.getSize()){
                T newArray[v.getSize()];
                for(int i=0; i<size; i++){
                    newArray[i]=values[i];
                }
                for(int i=size; i<v.getSize(); i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, v.getSize());
               
                //Insert code here
                double dp = 0;    
                for(int i = 0; i < v.getSize(); i++)
                    { dp += newV[i] * v[i]; }

                return dp;
            }else{
                T newArray[size];
                for(int i=0; i<v.getSize(); i++){
                    newArray[i]=v[i];
                }
                for(int i=v.getSize(); i<size; i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, size);
                //Insert code here
                double dp = 0;    
                for(int i = 0; i < size; i++)
                    { dp += values[i] * newV[i]; }

                return dp;

            }
        }        
        double dp = 0;    
        for(int i = 0; i < size; i++)
            { dp += values[i] * v[i]; }
    
        return dp;
    }            
    




    //Subtract one vector from another
   template <typename T> const M_Vector<T> M_Vector<T>::operator- (const M_Vector<T> & v) const{
            if(size!=v.getSize()){
	            if(size<v.getSize()){
                T newArray[v.getSize()];
                for(int i=0; i<size; i++){
                    newArray[i]=values[i];
                }
                for(int i=size; i<v.getSize(); i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, v.getSize());
               
                //Insert code here
                T returnArray[v.getSize()];
                for(int i=0; i<v.getSize(); i++){
                    returnArray[i] = newV[i]-v[i];
                }
                return M_Vector(returnArray, v.getSize());

            }else{
                T newArray[size];
                for(int i=0; i<v.getSize(); i++){
                    newArray[i]=v[i];
                }
                for(int i=v.getSize(); i<size; i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, size);
                //Insert code here
                T returnArray[size];
                for(int i=0; i<size; i++){
                    returnArray[i] = values[i]-newV[i];
                }
                return M_Vector(returnArray, size);
                

            }
        } 
        T returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]-v[i];
        }
        return M_Vector(returnArray, size);
    }   
    

    //Subtrats the left and right vectors and sets the left vector to the difference
    template <typename T> void M_Vector<T>::operator-= (const M_Vector<T> & v){
            if(size!=v.getSize()){
	            if(size<v.getSize()){
                T newArray[v.getSize()];
                for(int i=0; i<size; i++){
                    newArray[i]=values[i];
                }
                for(int i=size; i<v.getSize(); i++){
                    newArray[i]=0;  
                }   
                values = newArray;
            }else{
                T newArray[size];
                for(int i=0; i<v.getSize(); i++){
                    newArray[i]=v[i];
                }
                for(int i=v.getSize(); i<size; i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, size);
                for(int i=0; i<size; i++){
                    values[i] = values[i]-newV[i];
                }       
                return;
            }
        } 
        for(int i=0; i<size; i++){
            values[i] = values[i]-v[i];
        }
}	 






    //Add one vector to another
    template <typename T> const M_Vector<T> M_Vector<T>::operator+ (const M_Vector<T> & v) const{
                    if(size!=v.getSize()){
	            if(size<v.getSize()){
                T newArray[v.getSize()];
                for(int i=0; i<size; i++){
                    newArray[i]=values[i];
                }
                for(int i=size; i<v.getSize(); i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, v.getSize());
               
                //Insert code here
                T returnArray[v.getSize()];
                for(int i=0; i<v.getSize(); i++){
                    returnArray[i] = newV[i]+v[i];
                }
                return M_Vector(returnArray, v.getSize());
        return M_Vector(returnArray, size);

            }else{
                T newArray[size];
                for(int i=0; i<v.getSize(); i++){
                    newArray[i]=v[i];
                }
                for(int i=v.getSize(); i<size; i++){
                    newArray[i]=0;  
                }
                M_Vector newV = M_Vector(newArray, size);
                //Insert code here
                T returnArray[size];
                for(int i=0; i<size; i++){
                    returnArray[i] = values[i]+newV[i];
                }
                return M_Vector(returnArray, size);

            }
        } 
        T returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]+v[i];
        }
        return M_Vector(returnArray, size);
    }   
                        

    //Adds the left and right vectors together and sets the left vector to the sum
     template <typename T> void M_Vector<T>::operator+= (const M_Vector<T> & v){
           if(size!=v.getSize()){
	            if(size<v.getSize()){
                    T newArray[v.getSize()];
                    for(int i=0; i<size; i++){
                        newArray[i]=values[i];
                    }
                    for(int i=size; i<v.getSize(); i++){
                        newArray[i]=0;  
                    }
                    values = newArray;
                   
                }
              }else{
                    T newArray[size];
                    for(int i=0; i<v.getSize(); i++){
                        newArray[i]=v[i];
                    }
                    for(int i=v.getSize(); i<size; i++){
                        newArray[i]=0;  
                    }
                    M_Vector newV = M_Vector(newArray, size);
                    for(int i=0; i<size; i++){
                       values[i] = values[i]+newV[i];
                    }
                    return;
                }
        for(int i=0; i<size; i++){
           values[i] = values[i]+v[i];
        }
    } 






    //Divide vector by a double
    template <typename T> const M_Vector<T> M_Vector<T>::operator/ (const T dbl) const{
        if(dbl==0 || dbl==-0)
            return M_Vector<T>();        
        T returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]/dbl;
        }
        return M_Vector(returnArray, size);     
    }
    
    //Divide vector by a double and sets the left vector as the answer
    template <typename T> void M_Vector<T>::operator/= (const T dbl){ 
        if(dbl==0 || dbl==-0)
            return;        
        for(int i=0; i<size; i++){
            values[i] = values[i]/dbl;
        }
    }

    //Divide vector by an int
    template <typename T> const M_Vector<T> M_Vector<T>::operator/ (const int I) const{
        if(I==0)
            return M_Vector();
        T returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]/I;
        }
        return M_Vector(returnArray, size);
        
    }    
    
    //Divide vector by an int and sets the left vector as the answer
    template <typename T> void M_Vector<T>::operator/= (const int I){
        if(I==0)
            return;
        for(int i=0; i<size; i++){
            values[i] = values[i]/I;
        }
    }                  






    //Multiply vector by a double
    template <typename T> const M_Vector<T> M_Vector<T>::operator* (const T dbl) const{
        T returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]*dbl;
        }
        return M_Vector(returnArray, size);
    }

    //Multiply vector by a double and sets the left vector as the product
    template <typename T> void M_Vector<T>::operator*= (const T dbl){
        for(int i=0; i<size; i++){
            values[i] = values[i]*dbl;
        }
    }

    //Multiply vector by an int    
    template <typename T> const M_Vector<T> M_Vector<T>::operator* (const int I) const{
        T returnArray[size];
        for(int i=0; i<size; i++){
            returnArray[i] = values[i]*I;
        }
        return M_Vector(returnArray, size);
    }   

    //Multiply vector by an int and sets the letf vector as the product
    template <typename T> void M_Vector<T> ::operator*= (const int I){
        for(int i=0; i<size; i++){
            values[i] = values[i]*I;
        }
    }




    //Checks to see if 2 vectors are equal
    template <typename T> bool M_Vector<T>::operator== (const M_Vector<T> & v) const{
        if(size!=v.getSize())  
            return false;        
        bool equal = true;
        for(int i=0; i<size; i++){
            if(values[i]!=v[i]){
                equal = false;
                break;
            }
        }
        return equal;
    }     

    //Checks to see if 2 vectors are unequal
    template <typename T> bool M_Vector<T>::operator!= (const M_Vector<T> & v) const{

        return !(*this==v);
    }     





    //Assigns one vector to another
    template <typename T> const M_Vector<T>&  M_Vector<T>::operator= (const M_Vector<T> & v){
        if(v == *this)
            return *this;
        delete[] values;        
        values = new T[v.getSize()];
        for(int i=0; i<v.getSize(); i++){
                
           values[i] = v[i];
            

        }
        
        size=v.getSize();
        
        return *this;    
    }

    //Destructor
    template <typename T> M_Vector<T>::~M_Vector(){
        delete[] values;
    }


#endif

