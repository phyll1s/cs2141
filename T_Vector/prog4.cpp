/********************************************************************
 * prog4.cpp
 * Version 1.1
 * Written by Paul Bonamy
 * Last update: 5 November 2009
 * 
 * Demonstration of the proper functioning of an M_Vector class template,
 * provided as part of Homeword #4 for CS2141, Fall 2009
 *
 * This program does not provide an exhaustive test of the M_Vector.
 * It is intended, rather, to provide a reasonable demonstration
 * of the class's functionality.
 *
 * While you may modify this file for the purposes of testing your
 * program, please be sure to a) submit an unmodifed copy with your
 * program and b) write your class to be independent of the details
 * of main's implementation. I should be able to use your class with
 * any version of main.cpp that respects the same class interface.
 *
 * This code should be error free. In the event that it is not,
 * please send me an email (pjbonamy@mtu.edu)
 * ******************************************************************/

#include <iostream>
#include "M_Vector.h"

using namespace std;

int main() {
    cout << "Constructor tests:" << endl;
    // creation from double array
    double a_values[] = {1.1, 2.2, 3.3, 4.4, 5.5, 6.6};
    M_Vector<double> a(a_values, 6);
    cout << "M_Vector a should have (1.1, 2.2, 3.3, 4.4, 5.5, 6.6). It actually has: " << a << endl;

    // zero fill
    M_Vector<double> b(6);
    cout << "M_Vector b should have (0, 0, 0, 0, 0, 0). It actually has: " << b << endl;
    
    // empty
    M_Vector<double> c;
    cout << "M_Vector c should have (). It actually has: " << c << endl;

    // copy
    M_Vector<double> d(b);
    cout << "M_Vector d should have (0, 0, 0, 0, 0, 0). It actually has: " << d << endl;
    
    // asignment test
    cout << endl << "Assignment test:" << endl;
    M_Vector<double> k;
    k = k = a;
    cout << "M_Vector k should have (1.1, 2.2, 3.3, 4.4, 5.5, 6.6). It actually has: " << k << endl;
    
    // [] test
    cout << endl << "Operator [] test:" << endl;
    for (int i = 0; i < a.getSize(); i++) {
        b[b.getSize() - i - 1] = a[i];
    }
    cout << "b should now have (6.6, 5.5, 4.4, 3.3, 2.2, 1.1). It actually has: " << b << endl;
    cout << "d should now have (0, 0, 0, 0, 0, 0). It actually has: " << d << endl;
    
    cout << endl << "M_Vector-M_Vector mathematics tests:" << endl;
    // dot product
    double e = a * b;
    cout << "e should have value 67.76. It actually has: " << e << endl;
    M_Vector<double> l(a_values, 4);
    e = a * l;
    cout << "e should now have value 36.3. It actually has: " << e << endl;
    
    // addition
    M_Vector<double> f = a + b;
    cout << "f should now have (7.7, 7.7, 7.7, 7.7, 7.7, 7.7). It actually has: " << f << endl;
    f += a;
    cout << "f should now have (8.8, 9.9, 11, 12.1, 13.2, 14.3). It actually has: " << f << endl;
    M_Vector<double> g = a + l;
    cout << "g should have value (2.2, 4.4, 6.6, 8.8, 5.5, 6.6). It actually has: " << g << endl;
    g += l;
    cout << "g should have value (3.3, 6.6, 9.9, 13.2, 5.5, 6.6). It actually has: " << g << endl;
    
    // subtraction
    M_Vector<double> h = a - b;
    cout << "h should now have (-5.5, -3.3, -1.1, 1.1, 3.3, 5.5). It actually has: " << h << endl;
    h -= a;
    cout << "h should now have (-6.6, -5.5, -4.4, -3.3, -2.2, -1.1). It actually has: " << h << endl;
    M_Vector<double> i = a - l;
    cout << "i should have value (0, 0, 0, 0, 5.5, 6.6). It actually has: " << i << endl;
    i -= l;
    cout << "i should have value (-1.1, -2.2, -3.3, -4.4, 5.5, 6.6). It actually has: " << i << endl;
    
    cout << endl << "M_Vector-Scalar mathematics tests:" << endl;
    // multiplication
    a = a * 2;
    cout << "a should now have (2.2, 4.4, 6.6, 8.8, 11, 13.2). It actually has: "  << a << endl;
    a *= 2;
    cout << "a should now have (4.4, 8.8, 13.2, 17.6, 22, 26.4). It actually has: "  << a << endl;
    
    // division
    a = a / 2;
    cout << "a should now have (2.2, 4.4, 6.6, 8.8, 11, 13.2). It actually has: "  << a << endl;
    a /= 4;
    cout << "a should now have (0.55, 1.1, 1.65, 2.2, 2.75, 3.3). It actually has: "  << a << endl;
    cout << "k should now have (1.1, 2.2, 3.3, 4.4, 5.5, 6.6). It actually has: "  << k << endl;
    
    cout << endl << "Equality tests:" << endl;
    M_Vector<double> j(6);
    cout << "a == b should be 0. It's actually: " << (a == b) << endl;
    cout << "a == g should be 0. It's actually: " << (a == g) << endl;
    cout << "d == j should be 1. It's actually: " << (d == j) << endl;
    cout << "g == i should be 0. It's actually: " << (g == i) << endl;
    cout << "a != b should be 1. It's actually: " << (a != b) << endl;
    cout << "a != g should be 1. It's actually: " << (a != g) << endl;
    cout << "d != j should be 0. It's actually: " << (d != j) << endl;
    cout << "g != i should be 1. It's actually: " << (g != i) << endl;
    
    return 0;
}
